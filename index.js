/**
 * commons function
 * Any function Country.
 *
 */

'use strict';
module.exports = {
   /**
    * getCountryList function
    * Get all country with code and name.
    *
    * @return {Array} list of country
    */
   getCountryList: function() {
      return [
            { code: "AD", name: "Andorra" },
            { code: "AE", name: "United Arab Emirates" },
            { code: "AF", name: "Afghanistan" }, 
            { code: "AG", name: "Antigua and Barbuda" },
            { code: "AI", name: "Anguilla" },
            { code: "AL", name: "Albania" },
            { code: "AM", name: "Armenia" },
            { code: "AN", name: "Netherlands Antilles" },
            { code: "AO", name: "Angola" },
            { code: "AQ", name: "Antarctica" },
            { code: "AR", name: "Argentina" },
            { code: "AS", name: "American Samoa" },
            { code: "AT", name: "Austria" },
            { code: "AU", name: "Australia" },
            { code: "AW", name: "Aruba" },
            { code: "AX", name: "Aland Islands" },
            { code: "AZ", name: "Azerbaijan" },
            { code: "BA", name: "Bosnia and Herzegovina" },
            { code: "BB", name: "Barbados" },
            { code: "BD", name: "Bangladesh" },
            { code: "BE", name: "Belgium" },
            { code: "BF", name: "Burkina Faso" },
            { code: "BG", name: "Bulgaria" },
            { code: "BH", name: "Bahrain" },
            { code: "BI", name: "Burundi" },
            { code: "BJ", name: "Benin" },
            { code: "BL", name: "Saint Barthélemy" },
            { code: "BM", name: "Bermuda" },
            { code: "BN", name: "Brunei" },
            { code: "BO", name: "Bolivia" },
            { code: "BQ", name: "Bonaire, Saint Eustatius and Saba" },
            { code: "BR", name: "Brazil" },
            { code: "BS", name: "Bahamas" },
            { code: "BT", name: "Bhutan" },
            { code: "BV", name: "Bouvet Island" },
            { code: "BW", name: "Botswana" },
            { code: "BY", name: "Belarus" },
            { code: "BZ", name: "Belize" },
            { code: "CA", name: "Canada" },
            { code: "CC", name: "Cocos Islands" },
            { code: "CD", name: "Democratic Republic of the Congo" },
            { code: "CF", name: "Central African Republic" },
            { code: "CG", name: "Republic of the Congo" },
            { code: "CH", name: "Switzerland" },
            { code: "CI", name: "Ivory Coast" },
            { code: "CK", name: "Cook Islands" },
            { code: "CL", name: "Chile" },
            { code: "CM", name: "Cameroon" },
            { code: "CN", name: "China" },
            { code: "CO", name: "Colombia" },
            { code: "CR", name: "Costa Rica" },
            { code: "CS", name: "Serbia and Montenegro" },
            { code: "CU", name: "Cuba" },
            { code: "CV", name: "Cape Verde" },
            { code: "CW", name: "Curaçao" },
            { code: "CX", name: "Christmas Island" },
            { code: "CY", name: "Cyprus" },
            { code: "CZ", name: "Czech Republic" },
            { code: "DE", name: "Germany" },
            { code: "DJ", name: "Djibouti" },
            { code: "DK", name: "Denmark" },
            { code: "DM", name: "Dominica" },
            { code: "DO", name: "Dominican Republic" },
            { code: "DZ", name: "Algeria" },
            { code: "EC", name: "Ecuador" },
            { code: "EE", name: "Estonia" },
            { code: "EG", name: "Egypt" },
            { code: "EH", name: "Western Sahara" },
            { code: "ER", name: "Eritrea" },
            { code: "ES", name: "Spain" },
            { code: "ET", name: "Ethiopia" },
            { code: "FI", name: "Finland" },
            { code: "FJ", name: "Fiji" },
            { code: "FK", name: "Falkland Islands" },
            { code: "FM", name: "Micronesia" },
            { code: "FO", name: "Faroe Islands" },
            { code: "FR", name: "France" },
            { code: "GA", name: "Gabon" },
            { code: "GB", name: "United Kingdom" },
            { code: "GD", name: "Grenada" },
            { code: "GE", name: "Georgia" },
            { code: "GF", name: "French Guiana" },
            { code: "GG", name: "Guernsey" },
            { code: "GH", name: "Ghana" },
            { code: "GI", name: "Gibraltar" },
            { code: "GL", name: "Greenland" },
            { code: "GM", name: "Gambia" },
            { code: "GN", name: "Guinea" },
            { code: "GP", name: "Guadeloupe" },
            { code: "GQ", name: "Equatorial Guinea" },
            { code: "GR", name: "Greece" },
            { code: "GS", name: "South Georgia and the South Sandwich Islands" },
            { code: "GT", name: "Guatemala" },
            { code: "GU", name: "Guam" },
            { code: "GW", name: "Guinea-Bissau" },
            { code: "GY", name: "Guyana" },
            { code: "HK", name: "Hong Kong" },
            { code: "HM", name: "Heard Island and McDonald Islands" },
            { code: "HN", name: "Honduras" },
            { code: "HR", name: "Croatia" },
            { code: "HT", name: "Haiti" },
            { code: "HU", name: "Hungary" },
            { code: "ID", name: "Indonesia" },
            { code: "IE", name: "Ireland" },
            { code: "IL", name: "Israel" },
            { code: "IM", name: "Isle of Man" },
            { code: "IN", name: "India" },
            { code: "IO", name: "British Indian Ocean Territory" },
            { code: "IQ", name: "Iraq" },
            { code: "IR", name: "Iran" },
            { code: "IS", name: "Iceland" },
            { code: "IT", name: "Italy" },
            { code: "JE", name: "Jersey" },
            { code: "JM", name: "Jamaica" },
            { code: "JO", name: "Jordan" },
            { code: "JP", name: "Japan" },
            { code: "KE", name: "Kenya" },
            { code: "KG", name: "Kyrgyzstan" },
            { code: "KH", name: "Cambodia" },
            { code: "KI", name: "Kiribati" },
            { code: "KM", name: "Comoros" },
            { code: "KN", name: "Saint Kitts and Nevis" },
            { code: "KP", name: "North Korea" },
            { code: "KR", name: "South Korea" },
            { code: "KW", name: "Kuwait" },
            { code: "KY", name: "Cayman Islands" },
            { code: "KZ", name: "Kazakhstan" },
            { code: "LA", name: "Laos" },
            { code: "LB", name: "Lebanon" },
            { code: "LC", name: "Saint Lucia" },
            { code: "LI", name: "Liechtenstein" },
            { code: "LK", name: "Sri Lanka" },
            { code: "LR", name: "Liberia" },
            { code: "LS", name: "Lesotho" },
            { code: "LT", name: "Lithuania" },
            { code: "LU", name: "Luxembourg" },
            { code: "LV", name: "Latvia" },
            { code: "LY", name: "Libya" },
            { code: "MA", name: "Morocco" },
            { code: "MC", name: "Monaco" },
            { code: "MD", name: "Moldova" },
            { code: "ME", name: "Montenegro" },
            { code: "MF", name: "Saint Martin" },
            { code: "MG", name: "Madagascar" },
            { code: "MH", name: "Marshall Islands" },
            { code: "MK", name: "Macedonia" },
            { code: "ML", name: "Mali" },
            { code: "MM", name: "Myanmar" },
            { code: "MN", name: "Mongolia" },
            { code: "MO", name: "Macao" },
            { code: "MP", name: "Northern Mariana Islands" },
            { code: "MQ", name: "Martinique" },
            { code: "MR", name: "Mauritania" },
            { code: "MS", name: "Montserrat" },
            { code: "MT", name: "Malta" },
            { code: "MU", name: "Mauritius" },
            { code: "MV", name: "Maldives" },
            { code: "MW", name: "Malawi" },
            { code: "MX", name: "Mexico" },
            { code: "MY", name: "Malaysia" },
            { code: "MZ", name: "Mozambique" },
            { code: "NA", name: "Namibia" },
            { code: "NC", name: "New Caledonia" },
            { code: "NE", name: "Niger" },
            { code: "NF", name: "Norfolk Island" },
            { code: "NG", name: "Nigeria" },
            { code: "NI", name: "Nicaragua" },
            { code: "NL", name: "Netherlands" },
            { code: "NO", name: "Norway" },
            { code: "NP", name: "Nepal" },
            { code: "NR", name: "Nauru" },
            { code: "NU", name: "Niue" },
            { code: "NZ", name: "New Zealand" },
            { code: "OM", name: "Oman" },
            { code: "PA", name: "Panama" },
            { code: "PE", name: "Peru" },
            { code: "PF", name: "French Polynesia" },
            { code: "PG", name: "Papua New Guinea" },
            { code: "PH", name: "Philippines" },
            { code: "PK", name: "Pakistan" },
            { code: "PL", name: "Poland" },
            { code: "PM", name: "Saint Pierre and Miquelon" },
            { code: "PN", name: "Pitcairn" },
            { code: "PR", name: "Puerto Rico" },
            { code: "PS", name: "Palestinian Territory" },
            { code: "PT", name: "Portugal" },
            { code: "PW", name: "Palau" },
            { code: "PY", name: "Paraguay" },
            { code: "QA", name: "Qatar" },
            { code: "RE", name: "Reunion" },
            { code: "RO", name: "Romania" },
            { code: "RS", name: "Serbia" },
            { code: "RU", name: "Russia" },
            { code: "RW", name: "Rwanda" },
            { code: "SA", name: "Saudi Arabia" },
            { code: "SB", name: "Solomon Islands" },
            { code: "SC", name: "Seychelles" },
            { code: "SD", name: "Sudan" },
            { code: "SE", name: "Sweden" },
            { code: "SG", name: "Singapore" },
            { code: "SH", name: "Saint Helena" },
            { code: "SI", name: "Slovenia" },
            { code: "SJ", name: "Svalbard and Jan Mayen" },
            { code: "SK", name: "Slovakia" },
            { code: "SL", name: "Sierra Leone" },
            { code: "SM", name: "San Marino" },
            { code: "SN", name: "Senegal" },
            { code: "SO", name: "Somalia" },
            { code: "SR", name: "Suriname" },
            { code: "SS", name: "South Sudan" },
            { code: "ST", name: "Sao Tome and Principe" },
            { code: "SV", name: "El Salvador" },
            { code: "SX", name: "Sint Maarten" },
            { code: "SY", name: "Syria" },
            { code: "SZ", name: "Swaziland" },
            { code: "TC", name: "Turks and Caicos Islands" },
            { code: "TD", name: "Chad" },
            { code: "TF", name: "French Southern Territories" },
            { code: "TG", name: "Togo" },
            { code: "TH", name: "Thailand" },
            { code: "TJ", name: "Tajikistan" },
            { code: "TK", name: "Tokelau" },
            { code: "TL", name: "East Timor" },
            { code: "TM", name: "Turkmenistan" },
            { code: "TN", name: "Tunisia" },
            { code: "TO", name: "Tonga" },
            { code: "TR", name: "Turkey" },
            { code: "TT", name: "Trinidad and Tobago" },
            { code: "TV", name: "Tuvalu" },
            { code: "TW", name: "Taiwan" },
            { code: "TZ", name: "Tanzania" },
            { code: "UA", name: "Ukraine" },
            { code: "UG", name: "Uganda" },
            { code: "UM", name: "United States Minor Outlying Islands" },
            { code: "US", name: "United States" },
            { code: "UY", name: "Uruguay" },
            { code: "UZ", name: "Uzbekistan" },
            { code: "VA", name: "Vatican" },
            { code: "VC", name: "Saint Vincent and the Grenadines" },
            { code: "VE", name: "Venezuela" },
            { code: "VG", name: "British Virgin Islands" },
            { code: "VI", name: "U.S. Virgin Islands" },
            { code: "VN", name: "Vietnam" },
            { code: "VU", name: "Vanuatu" },
            { code: "WF", name: "Wallis and Futuna" },
            { code: "WS", name: "Samoa" },
            { code: "XK", name: "Kosovo" },
            { code: "YE", name: "Yemen" },
            { code: "YT", name: "Mayotte" },
            { code: "ZA", name: "South Africa" },
            { code: "ZM", name: "Zambia" },
            { code: "ZW", name: "Zimbabwe" }
         ].sort(function(a,b) {
            if (a.name > b.name)
               return 1;

            if (a.name < b.name)
               return -1;

            return 0;
         });
   },
   /**
    * getCountryForTimezone function
    * return the timezone of the country
    *
    * @return {Object} Timezone of country
    */
   getCountryForTimezone: function(timezone) {
      var countryByTimezone = {
         'Europe/Andorra': 'AD',
         'Asia/Dubai': 'AE',
         'Asia/Kabul': 'AF',
         'America/Antigua': 'AG',
         'America/Anguilla': 'AI',
         'Europe/Tirane': 'AL',
         'Asia/Yerevan': 'AM',
         'Africa/Luanda': 'AO',
         'Antarctica/McMurdo': 'AQ',
         'Antarctica/Rothera': 'AQ',
         'Antarctica/Palmer': 'AQ',
         'Antarctica/Mawson': 'AQ',
         'Antarctica/Davis': 'AQ',
         'Antarctica/Casey': 'AQ',
         'Antarctica/Vostok': 'AQ',
         'Antarctica/DumontDUrville': 'AQ',
         'Antarctica/Syowa': 'AQ',
         'Antarctica/Troll': 'AQ',
         'America/Argentina/Buenos_Aires': 'AR',
         'America/Argentina/Cordoba': 'AR',
         'America/Argentina/Salta': 'AR',
         'America/Argentina/Jujuy': 'AR',
         'America/Argentina/Tucuman': 'AR',
         'America/Argentina/Catamarca': 'AR',
         'America/Argentina/La_Rioja': 'AR',
         'America/Argentina/San_Juan': 'AR',
         'America/Argentina/Mendoza': 'AR',
         'America/Argentina/San_Luis': 'AR',
         'America/Argentina/Rio_Gallegos': 'AR',
         'America/Argentina/Ushuaia': 'AR',
         'Pacific/Pago_Pago': 'AS',
         'Europe/Vienna': 'AT',
         'Australia/Lord_Howe': 'AU',
         'Antarctica/Macquarie': 'AU',
         'Australia/Hobart': 'AU',
         'Australia/Currie': 'AU',
         'Australia/Melbourne': 'AU',
         'Australia/Sydney': 'AU',
         'Australia/Broken_Hill': 'AU',
         'Australia/Brisbane': 'AU',
         'Australia/Lindeman': 'AU',
         'Australia/Adelaide': 'AU',
         'Australia/Darwin': 'AU',
         'Australia/Perth': 'AU',
         'Australia/Eucla': 'AU',
         'America/Aruba': 'AW',
         'Europe/Mariehamn': 'AX',
         'Asia/Baku': 'AZ',
         'Europe/Sarajevo': 'BA',
         'America/Barbados': 'BB',
         'Asia/Dhaka': 'BD',
         'Europe/Brussels': 'BE',
         'Africa/Ouagadougou': 'BF',
         'Europe/Sofia': 'BG',
         'Asia/Bahrain': 'BH',
         'Africa/Bujumbura': 'BI',
         'Africa/Porto-Novo': 'BJ',
         'America/St_Barthelemy': 'BL',
         'Atlantic/Bermuda': 'BM',
         'Asia/Brunei': 'BN',
         'America/La_Paz': 'BO',
         'America/Kralendijk': 'BQ',
         'America/Noronha': 'BR',
         'America/Belem': 'BR',
         'America/Fortaleza': 'BR',
         'America/Recife': 'BR',
         'America/Araguaina': 'BR',
         'America/Maceio': 'BR',
         'America/Bahia': 'BR',
         'America/Sao_Paulo': 'BR',
         'America/Campo_Grande': 'BR',
         'America/Cuiaba': 'BR',
         'America/Santarem': 'BR',
         'America/Porto_Velho': 'BR',
         'America/Boa_Vista': 'BR',
         'America/Manaus': 'BR',
         'America/Eirunepe': 'BR',
         'America/Rio_Branco': 'BR',
         'America/Nassau': 'BS',
         'Asia/Thimphu': 'BT',
         'Africa/Gaborone': 'BW',
         'Europe/Minsk': 'BY',
         'America/Belize': 'BZ',
         'America/St_Johns': 'CA',
         'America/Halifax': 'CA',
         'America/Glace_Bay': 'CA',
         'America/Moncton': 'CA',
         'America/Goose_Bay': 'CA',
         'America/Blanc-Sablon': 'CA',
         'America/Toronto': 'CA',
         'America/Nipigon': 'CA',
         'America/Thunder_Bay': 'CA',
         'America/Iqaluit': 'CA',
         'America/Pangnirtung': 'CA',
         'America/Resolute': 'CA',
         'America/Atikokan': 'CA',
         'America/Rankin_Inlet': 'CA',
         'America/Winnipeg': 'CA',
         'America/Rainy_River': 'CA',
         'America/Regina': 'CA',
         'America/Swift_Current': 'CA',
         'America/Edmonton': 'CA',
         'America/Cambridge_Bay': 'CA',
         'America/Yellowknife': 'CA',
         'America/Inuvik': 'CA',
         'America/Creston': 'CA',
         'America/Dawson_Creek': 'CA',
         'America/Vancouver': 'CA',
         'America/Whitehorse': 'CA',
         'America/Dawson': 'CA',
         'Indian/Cocos': 'CC',
         'Africa/Kinshasa': 'CD',
         'Africa/Lubumbashi': 'CD',
         'Africa/Bangui': 'CF',
         'Africa/Brazzaville': 'CG',
         'Europe/Zurich': 'CH',
         'Africa/Abidjan': 'CI',
         'Pacific/Rarotonga': 'CK',
         'America/Santiago': 'CL',
         'Pacific/Easter': 'CL',
         'Africa/Douala': 'CM',
         'Asia/Shanghai': 'CN',
         'Asia/Urumqi': 'CN',
         'America/Bogota': 'CO',
         'America/Costa_Rica': 'CR',
         'America/Havana': 'CU',
         'Atlantic/Cape_Verde': 'CV',
         'America/Curacao': 'CW',
         'Indian/Christmas': 'CX',
         'Asia/Nicosia': 'CY',
         'Europe/Prague': 'CZ',
         'Europe/Berlin': 'DE',
         'Europe/Busingen': 'DE',
         'Africa/Djibouti': 'DJ',
         'Europe/Copenhagen': 'DK',
         'America/Dominica': 'DM',
         'America/Santo_Domingo': 'DO',
         'Africa/Algiers': 'DZ',
         'America/Guayaquil': 'EC',
         'Pacific/Galapagos': 'EC',
         'Europe/Tallinn': 'EE',
         'Africa/Cairo': 'EG',
         'Africa/El_Aaiun': 'EH',
         'Africa/Asmara': 'ER',
         'Europe/Madrid': 'ES',
         'Africa/Ceuta': 'ES',
         'Atlantic/Canary': 'ES',
         'Africa/Addis_Ababa': 'ET',
         'Europe/Helsinki': 'FI',
         'Pacific/Fiji': 'FJ',
         'Atlantic/Stanley': 'FK',
         'Pacific/Chuuk': 'FM',
         'Pacific/Pohnpei': 'FM',
         'Pacific/Kosrae': 'FM',
         'Atlantic/Faroe': 'FO',
         'Europe/Paris': 'FR',
         'Africa/Libreville': 'GA',
         'Europe/London': 'GB',
         'America/Grenada': 'GD',
         'Asia/Tbilisi': 'GE',
         'America/Cayenne': 'GF',
         'Europe/Guernsey': 'GG',
         'Africa/Accra': 'GH',
         'Europe/Gibraltar': 'GI',
         'America/Godthab': 'GL',
         'America/Danmarkshavn': 'GL',
         'America/Scoresbysund': 'GL',
         'America/Thule': 'GL',
         'Africa/Banjul': 'GM',
         'Africa/Conakry': 'GN',
         'America/Guadeloupe': 'GP',
         'Africa/Malabo': 'GQ',
         'Europe/Athens': 'GR',
         'Atlantic/South_Georgia': 'GS',
         'America/Guatemala': 'GT',
         'Pacific/Guam': 'GU',
         'Africa/Bissau': 'GW',
         'America/Guyana': 'GY',
         'Asia/Hong_Kong': 'HK',
         'America/Tegucigalpa': 'HN',
         'Europe/Zagreb': 'HR',
         'America/Port-au-Prince': 'HT',
         'Europe/Budapest': 'HU',
         'Asia/Jakarta': 'ID',
         'Asia/Pontianak': 'ID',
         'Asia/Makassar': 'ID',
         'Asia/Jayapura': 'ID',
         'Europe/Dublin': 'IE',
         'Asia/Jerusalem': 'IL',
         'Europe/Isle_of_Man': 'IM',
         'Asia/Kolkata': 'IN',
         'Indian/Chagos': 'IO',
         'Asia/Baghdad': 'IQ',
         'Asia/Tehran': 'IR',
         'Atlantic/Reykjavik': 'IS',
         'Europe/Rome': 'IT',
         'Europe/Jersey': 'JE',
         'America/Jamaica': 'JM',
         'Asia/Amman': 'JO',
         'Asia/Tokyo': 'JP',
         'Africa/Nairobi': 'KE',
         'Asia/Bishkek': 'KG',
         'Asia/Phnom_Penh': 'KH',
         'Pacific/Tarawa': 'KI',
         'Pacific/Enderbury': 'KI',
         'Pacific/Kiritimati': 'KI',
         'Indian/Comoro': 'KM',
         'America/St_Kitts': 'KN',
         'Asia/Pyongyang': 'KP',
         'Asia/Seoul': 'KR',
         'Asia/Kuwait': 'KW',
         'America/Cayman': 'KY',
         'Asia/Almaty': 'KZ',
         'Asia/Qyzylorda': 'KZ',
         'Asia/Aqtobe': 'KZ',
         'Asia/Aqtau': 'KZ',
         'Asia/Oral': 'KZ',
         'Asia/Vientiane': 'LA',
         'Asia/Beirut': 'LB',
         'America/St_Lucia': 'LC',
         'Europe/Vaduz': 'LI',
         'Asia/Colombo': 'LK',
         'Africa/Monrovia': 'LR',
         'Africa/Maseru': 'LS',
         'Europe/Vilnius': 'LT',
         'Europe/Luxembourg': 'LU',
         'Europe/Riga': 'LV',
         'Africa/Tripoli': 'LY',
         'Africa/Casablanca': 'MA',
         'Europe/Monaco': 'MC',
         'Europe/Chisinau': 'MD',
         'Europe/Podgorica': 'ME',
         'America/Marigot': 'MF',
         'Indian/Antananarivo': 'MG',
         'Pacific/Majuro': 'MH',
         'Pacific/Kwajalein': 'MH',
         'Europe/Skopje': 'MK',
         'Africa/Bamako': 'ML',
         'Asia/Rangoon': 'MM',
         'Asia/Ulaanbaatar': 'MN',
         'Asia/Hovd': 'MN',
         'Asia/Choibalsan': 'MN',
         'Asia/Macau': 'MO',
         'Pacific/Saipan': 'MP',
         'America/Martinique': 'MQ',
         'Africa/Nouakchott': 'MR',
         'America/Montserrat': 'MS',
         'Europe/Malta': 'MT',
         'Indian/Mauritius': 'MU',
         'Indian/Maldives': 'MV',
         'Africa/Blantyre': 'MW',
         'America/Mexico_City': 'MX',
         'America/Cancun': 'MX',
         'America/Merida': 'MX',
         'America/Monterrey': 'MX',
         'America/Matamoros': 'MX',
         'America/Mazatlan': 'MX',
         'America/Chihuahua': 'MX',
         'America/Ojinaga': 'MX',
         'America/Hermosillo': 'MX',
         'America/Tijuana': 'MX',
         'America/Santa_Isabel': 'MX',
         'America/Bahia_Banderas': 'MX',
         'Asia/Kuala_Lumpur': 'MY',
         'Asia/Kuching': 'MY',
         'Africa/Maputo': 'MZ',
         'Africa/Windhoek': 'NA',
         'Pacific/Noumea': 'NC',
         'Africa/Niamey': 'NE',
         'Pacific/Norfolk': 'NF',
         'Africa/Lagos': 'NG',
         'America/Managua': 'NI',
         'Europe/Amsterdam': 'NL',
         'Europe/Oslo': 'NO',
         'Asia/Kathmandu': 'NP',
         'Pacific/Nauru': 'NR',
         'Pacific/Niue': 'NU',
         'Pacific/Auckland': 'NZ',
         'Pacific/Chatham': 'NZ',
         'Asia/Muscat': 'OM',
         'America/Panama': 'PA',
         'America/Lima': 'PE',
         'Pacific/Tahiti': 'PF',
         'Pacific/Marquesas': 'PF',
         'Pacific/Gambier': 'PF',
         'Pacific/Port_Moresby': 'PG',
         'Pacific/Bougainville': 'PG',
         'Asia/Manila': 'PH',
         'Asia/Karachi': 'PK',
         'Europe/Warsaw': 'PL',
         'America/Miquelon': 'PM',
         'Pacific/Pitcairn': 'PN',
         'America/Puerto_Rico': 'PR',
         'Asia/Gaza': 'PS',
         'Asia/Hebron': 'PS',
         'Europe/Lisbon': 'PT',
         'Atlantic/Madeira': 'PT',
         'Atlantic/Azores': 'PT',
         'Pacific/Palau': 'PW',
         'America/Asuncion': 'PY',
         'Asia/Qatar': 'QA',
         'Indian/Reunion': 'RE',
         'Europe/Bucharest': 'RO',
         'Europe/Belgrade': 'RS',
         'Europe/Kaliningrad': 'RU',
         'Europe/Moscow': 'RU',
         'Europe/Simferopol': 'RU',
         'Europe/Volgograd': 'RU',
         'Europe/Samara': 'RU',
         'Asia/Yekaterinburg': 'RU',
         'Asia/Omsk': 'RU',
         'Asia/Novosibirsk': 'RU',
         'Asia/Novokuznetsk': 'RU',
         'Asia/Krasnoyarsk': 'RU',
         'Asia/Irkutsk': 'RU',
         'Asia/Chita': 'RU',
         'Asia/Yakutsk': 'RU',
         'Asia/Khandyga': 'RU',
         'Asia/Vladivostok': 'RU',
         'Asia/Sakhalin': 'RU',
         'Asia/Ust-Nera': 'RU',
         'Asia/Magadan': 'RU',
         'Asia/Srednekolymsk': 'RU',
         'Asia/Kamchatka': 'RU',
         'Asia/Anadyr': 'RU',
         'Africa/Kigali': 'RW',
         'Asia/Riyadh': 'SA',
         'Pacific/Guadalcanal': 'SB',
         'Indian/Mahe': 'SC',
         'Africa/Khartoum': 'SD',
         'Europe/Stockholm': 'SE',
         'Asia/Singapore': 'SG',
         'Atlantic/St_Helena': 'SH',
         'Europe/Ljubljana': 'SI',
         'Arctic/Longyearbyen': 'SJ',
         'Europe/Bratislava': 'SK',
         'Africa/Freetown': 'SL',
         'Europe/San_Marino': 'SM',
         'Africa/Dakar': 'SN',
         'Africa/Mogadishu': 'SO',
         'America/Paramaribo': 'SR',
         'Africa/Juba': 'SS',
         'Africa/Sao_Tome': 'ST',
         'America/El_Salvador': 'SV',
         'America/Lower_Princes': 'SX',
         'Asia/Damascus': 'SY',
         'Africa/Mbabane': 'SZ',
         'America/Grand_Turk': 'TC',
         'Africa/Ndjamena': 'TD',
         'Indian/Kerguelen': 'TF',
         'Africa/Lome': 'TG',
         'Asia/Bangkok': 'TH',
         'Asia/Dushanbe': 'TJ',
         'Pacific/Fakaofo': 'TK',
         'Asia/Dili': 'TL',
         'Asia/Ashgabat': 'TM',
         'Africa/Tunis': 'TN',
         'Pacific/Tongatapu': 'TO',
         'Europe/Istanbul': 'TR',
         'America/Port_of_Spain': 'TT',
         'Pacific/Funafuti': 'TV',
         'Asia/Taipei': 'TW',
         'Africa/Dar_es_Salaam': 'TZ',
         'Europe/Kiev': 'UA',
         'Europe/Uzhgorod': 'UA',
         'Europe/Zaporozhye': 'UA',
         'Africa/Kampala': 'UG',
         'Pacific/Johnston': 'UM',
         'Pacific/Midway': 'UM',
         'Pacific/Wake': 'UM',
         'America/New_York': 'US',
         'America/Detroit': 'US',
         'America/Kentucky/Louisville': 'US',
         'America/Kentucky/Monticello': 'US',
         'America/Indiana/Indianapolis': 'US',
         'America/Indiana/Vincennes': 'US',
         'America/Indiana/Winamac': 'US',
         'America/Indiana/Marengo': 'US',
         'America/Indiana/Petersburg': 'US',
         'America/Indiana/Vevay': 'US',
         'America/Chicago': 'US',
         'America/Indiana/Tell_City': 'US',
         'America/Indiana/Knox': 'US',
         'America/Menominee': 'US',
         'America/North_Dakota/Center': 'US',
         'America/North_Dakota/New_Salem': 'US',
         'America/North_Dakota/Beulah': 'US',
         'America/Denver': 'US',
         'America/Boise': 'US',
         'America/Phoenix': 'US',
         'America/Los_Angeles': 'US',
         'America/Metlakatla': 'US',
         'America/Anchorage': 'US',
         'America/Juneau': 'US',
         'America/Sitka': 'US',
         'America/Yakutat': 'US',
         'America/Nome': 'US',
         'America/Adak': 'US',
         'Pacific/Honolulu': 'US',
         'America/Montevideo': 'UY',
         'Asia/Samarkand': 'UZ',
         'Asia/Tashkent': 'UZ',
         'Europe/Vatican': 'VA',
         'America/St_Vincent': 'VC',
         'America/Caracas': 'VE',
         'America/Tortola': 'VG',
         'America/St_Thomas': 'VI',
         'Asia/Ho_Chi_Minh': 'VN',
         'Pacific/Efate': 'VU',
         'Pacific/Wallis': 'WF',
         'Pacific/Apia': 'WS',
         'Asia/Aden': 'YE',
         'Indian/Mayotte': 'YT',
         'Africa/Johannesburg': 'ZA',
         'Africa/Lusaka': 'ZM',
         'Africa/Harare': 'ZW',
      };

      return countryByTimezone[timezone];
   },
   /**
    * getCurrencyList function
    * return all the Currency of the country
    *
    * @return {Array} Currency list
    */
   getCurrencyList: function(){
      return [
         { code: "AED", name: "United Arab Emirates Dirham" },
         { code: "AFN", name: "Afghanistan Afghani" },
         { code: "ALL", name: "Albania Lek" },
         { code: "AMD", name: "Armenia Dram" },
         { code: "ANG", name: "Netherlands Antilles Guilder" },
         { code: "AOA", name: "Angola Kwanza" },
         { code: "ARS", name: "Argentina Peso" },
         { code: "AUD", name: "Australia Dollar" },
         { code: "AWG", name: "Aruba Guilder" },
         { code: "AZN", name: "Azerbaijan New Manat" },
         { code: "BAM", name: "Bosnia and Herzegovina Convertible Marka" },
         { code: "BBD", name: "Barbados Dollar" },
         { code: "BDT", name: "Bangladesh Taka" },
         { code: "BGN", name: "Bulgaria Lev" },
         { code: "BHD", name: "Bahrain Dinar" },
         { code: "BIF", name: "Burundi Franc" },
         { code: "BMD", name: "Bermuda Dollar" },
         { code: "BND", name: "Brunei Darussalam Dollar" },
         { code: "BOB", name: "Bolivia Boliviano" },
         { code: "BRL", name: "Brazil Real" },
         { code: "BSD", name: "Bahamas Dollar" },
         { code: "BTN", name: "Bhutan Ngultrum" },
         { code: "BWP", name: "Botswana Pula" },
         { code: "BYR", name: "Belarus Ruble" },
         { code: "BZD", name: "Belize Dollar" },
         { code: "CAD", name: "Canada Dollar" },
         { code: "CDF", name: "Congo/Kinshasa Franc" },
         { code: "CHF", name: "Switzerland Franc" },
         { code: "CLP", name: "Chile Peso" },
         { code: "CNY", name: "China Yuan Renminbi" },
         { code: "COP", name: "Colombia Peso" },
         { code: "CRC", name: "Costa Rica Colon" },
         { code: "CUC", name: "Cuba Convertible Peso" },
         { code: "CUP", name: "Cuba Peso" },
         { code: "CVE", name: "Cape Verde Escudo" },
         { code: "CZK", name: "Czech Republic Koruna" },
         { code: "DJF", name: "Djibouti Franc" },
         { code: "DKK", name: "Denmark Krone" },
         { code: "DOP", name: "Dominican Republic Peso" },
         { code: "DZD", name: "Algeria Dinar" },
         { code: "EGP", name: "Egypt Pound" },
         { code: "ERN", name: "Eritrea Nakfa" },
         { code: "ETB", name: "Ethiopia Birr" },
         { code: "EUR", name: "Euro Member Countries" },
         { code: "FJD", name: "Fiji Dollar" },
         { code: "FKP", name: "Falkland Islands (Malvinas) Pound" },
         { code: "GBP", name: "United Kingdom Pound" },
         { code: "GEL", name: "Georgia Lari" },
         { code: "GGP", name: "Guernsey Pound" },
         { code: "GHS", name: "Ghana Cedi" },
         { code: "GIP", name: "Gibraltar Pound" },
         { code: "GMD", name: "Gambia Dalasi" },
         { code: "GNF", name: "Guinea Franc" },
         { code: "GTQ", name: "Guatemala Quetzal" },
         { code: "GYD", name: "Guyana Dollar" },
         { code: "HKD", name: "Hong Kong Dollar" },
         { code: "HNL", name: "Honduras Lempira" },
         { code: "HRK", name: "Croatia Kuna" },
         { code: "HTG", name: "Haiti Gourde" },
         { code: "HUF", name: "Hungary Forint" },
         { code: "IDR", name: "Indonesia Rupiah" },
         { code: "ILS", name: "Israel Shekel" },
         { code: "IMP", name: "Isle of Man Pound" },
         { code: "INR", name: "India Rupee" },
         { code: "IQD", name: "Iraq Dinar" },
         { code: "IRR", name: "Iran Rial" },
         { code: "ISK", name: "Iceland Krona" },
         { code: "JEP", name: "Jersey Pound" },
         { code: "JMD", name: "Jamaica Dollar" },
         { code: "JOD", name: "Jordan Dinar" },
         { code: "JPY", name: "Japan Yen" },
         { code: "KES", name: "Kenya Shilling" },
         { code: "KGS", name: "Kyrgyzstan Som" },
         { code: "KHR", name: "Cambodia Riel" },
         { code: "KMF", name: "Comoros Franc" },
         { code: "KPW", name: "Korea (North) Won" },
         { code: "KRW", name: "Korea (South) Won" },
         { code: "KWD", name: "Kuwait Dinar" },
         { code: "KYD", name: "Cayman Islands Dollar" },
         { code: "KZT", name: "Kazakhstan Tenge" },
         { code: "LAK", name: "Laos Kip" },
         { code: "LBP", name: "Lebanon Pound" },
         { code: "LKR", name: "Sri Lanka Rupee" },
         { code: "LRD", name: "Liberia Dollar" },
         { code: "LSL", name: "Lesotho Loti" },
         { code: "LYD", name: "Libya Dinar" },
         { code: "MAD", name: "Morocco Dirham" },
         { code: "MDL", name: "Moldova Leu" },
         { code: "MGA", name: "Madagascar Ariary" },
         { code: "MKD", name: "Macedonia Denar" },
         { code: "MMK", name: "Myanmar (Burma) Kyat" },
         { code: "MNT", name: "Mongolia Tughrik" },
         { code: "MOP", name: "Macau Pataca" },
         { code: "MRO", name: "Mauritania Ouguiya" },
         { code: "MUR", name: "Mauritius Rupee" },
         { code: "MVR", name: "Maldives (Maldive Islands) Rufiyaa" },
         { code: "MWK", name: "Malawi Kwacha" },
         { code: "MXN", name: "Mexico Peso" },
         { code: "MYR", name: "Malaysia Ringgit" },
         { code: "MZN", name: "Mozambique Metical" },
         { code: "NAD", name: "Namibia Dollar" },
         { code: "NGN", name: "Nigeria Naira" },
         { code: "NIO", name: "Nicaragua Cordoba" },
         { code: "NOK", name: "Norway Krone" },
         { code: "NPR", name: "Nepal Rupee" },
         { code: "NZD", name: "New Zealand Dollar" },
         { code: "OMR", name: "Oman Rial" },
         { code: "PAB", name: "Panama Balboa" },
         { code: "PEN", name: "Peru Nuevo Sol" },
         { code: "PGK", name: "Papua New Guinea Kina" },
         { code: "PHP", name: "Philippines Peso" },
         { code: "PKR", name: "Pakistan Rupee" },
         { code: "PLN", name: "Poland Zloty" },
         { code: "PYG", name: "Paraguay Guarani" },
         { code: "QAR", name: "Qatar Riyal" },
         { code: "RON", name: "Romania New Leu" },
         { code: "RSD", name: "Serbia Dinar" },
         { code: "RUB", name: "Russia Ruble" },
         { code: "RWF", name: "Rwanda Franc" },
         { code: "SAR", name: "Saudi Arabia Riyal" },
         { code: "SBD", name: "Solomon Islands Dollar" },
         { code: "SCR", name: "Seychelles Rupee" },
         { code: "SDG", name: "Sudan Pound" },
         { code: "SEK", name: "Sweden Krona" },
         { code: "SGD", name: "Singapore Dollar" },
         { code: "SHP", name: "Saint Helena Pound" },
         { code: "SLL", name: "Sierra Leone Leone" },
         { code: "SOS", name: "Somalia Shilling" },
         { code: "SPL*':  'Seborga Luigino" },
         { code: "SRD", name: "Suriname Dollar" },
         { code: "STD", name: "São Tomé and Príncipe Dobra" },
         { code: "SVC", name: "El Salvador Colon" },
         { code: "SYP", name: "Syria Pound" },
         { code: "SZL", name: "Swaziland Lilangeni" },
         { code: "THB", name: "Thailand Baht" },
         { code: "TJS", name: "Tajikistan Somoni" },
         { code: "TMT", name: "Turkmenistan Manat" },
         { code: "TND", name: "Tunisia Dinar" },
         { code: "TOP", name: "Tonga Pa'anga" },
         { code: "TRY", name: "Turkey Lira" },
         { code: "TTD", name: "Trinidad and Tobago Dollar" },
         { code: "TVD", name: "Tuvalu Dollar" },
         { code: "TWD", name: "Taiwan New Dollar" },
         { code: "TZS", name: "Tanzania Shilling" },
         { code: "UAH", name: "Ukraine Hryvnia" },
         { code: "UGX", name: "Uganda Shilling" },
         { code: "USD", name: "United States Dollar" },
         { code: "UYU", name: "Uruguay Peso" },
         { code: "UZS", name: "Uzbekistan Som" },
         { code: "VEF", name: "Venezuela Bolivar" },
         { code: "VND", name: "Viet Nam Dong" },
         { code: "VUV", name: "Vanuatu Vatu" },
         { code: "WST", name: "Samoa Tala" },
         { code: "XAF", name: "Communauté Financière Africaine (BEAC) CFA Franc BEAC" },
         { code: "XCD", name: "East Caribbean Dollar" },
         { code: "XDR", name: "International Monetary Fund (IMF) Special Drawing Rights" },
         { code: "XOF", name: "Communauté Financière Africaine (BCEAO) Franc" },
         { code: "XPF", name: "Comptoirs Français du Pacifique (CFP) Franc" },
         { code: "YER", name: "Yemen Rial" },
         { code: "ZAR", name: "South Africa Rand" },
         { code: "ZMW", name: "Zambia Kwacha" },
         { code: "ZWD", name: "Zimbabwe Dollar" },
         ];
   }
};